import { render } from '@testing-library/react';
import HamburguerMenu from '../../../src/components/icons/HamburguerMenu';

describe('Prueba en <HamburguerMenu />', () => {
  test('Debe hacer match con el snapshot', () => {
    const { container } = render(<HamburguerMenu />);
    expect(container).toMatchSnapshot();
  });
});
