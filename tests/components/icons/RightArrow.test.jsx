import { render } from '@testing-library/react';
import RightArrow from '../../../src/components/icons/RightArrow';

describe('Prueba en RightArrow', () => {
  test('Debe hacer match con el snapshot', () => {
    const { container } = render(<RightArrow />);
    expect(container).toMatchSnapshot();
  });
});
