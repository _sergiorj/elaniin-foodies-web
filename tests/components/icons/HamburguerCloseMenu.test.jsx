import { render } from '@testing-library/react';
import HamburguerCloseMenu from '../../../src/components/icons/HamburguerCloseMenu';

describe('Prueba en <HamburguerCloseMenu />', () => {
  test('Debe hacer match con el snapshot', () => {
    const { container } = render(<HamburguerCloseMenu />);
    expect(container).toMatchSnapshot();
  });
});
