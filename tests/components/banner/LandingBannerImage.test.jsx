const { render } = require('@testing-library/react');
const {
  default: LandingBannerImage,
} = require('../../../src/components/banner/LandingBannerImage');

describe('Pruebas en <LadingBannerImage />', () => {
  test('Debe hacer match con el snapshot', () => {
    const { container } = render(<LandingBannerImage />);

    expect(container).toMatchSnapshot();
  });
});
