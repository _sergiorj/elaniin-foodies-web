import { render } from '@testing-library/react';
import MenuBannerImage from '../../../src/components/banner/MenuBannerImage';

describe('Pruebas en <MenuBannerImage />', () => {
  test('Debe hacer match con el snapshot', () => {
    const { container } = render(<MenuBannerImage />);

    expect(container).toMatchSnapshot();
  });
});
