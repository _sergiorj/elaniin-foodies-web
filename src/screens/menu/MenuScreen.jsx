import React from 'react';
import BannerScreen from '../../components/header/BannerScreen';
import Header from '../../components/header/Header';

import { menuTitleBanner } from '../../components/data/Titles';
import FoodCategorySearch from '../../components/menuscreen/FoodCategorySearch';
import FoodList from '../../components/menuscreen/FoodList';
import Footer from '../../components/footer/Footer';
import Pagination from '../../components/menuscreen/Pagination';
import useFetchApiMenu from '../../hooks/MenuHook/useFetchApiMenu';

export default function MenuScreen() {
  const { menuHeroData } = useFetchApiMenu();

  return (
    <section className="bg-[#F8F8F8]">
      <Header lightColor={false} />
      <BannerScreen
        lightColor={false}
        dataTitle={menuTitleBanner}
        data={menuHeroData}
      />
      <FoodCategorySearch />
      <FoodList />
      <Pagination />
      <Footer />
    </section>
  );
}
