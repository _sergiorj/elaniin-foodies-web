import React from 'react';
import WhoIsFoodies from '../../components/foodies/WhoIsFoodies';
import BannerScreen from '../../components/header/BannerScreen';
import Header from '../../components/header/Header';

import {
  foodiesTitleBanner,
  foodiesSubTitleBanner,
} from '../../components/data/Titles';
import SliderComponent from '../../components/slider/SliderComponent';
import FoodiesApp from '../../components/foodiesapp/FoodiesApp';
import Footer from '../../components/footer/Footer';
import FeedbackForm from '../../components/form/FeedbackForm';
import SearchPlaces from '../../components/searchplaces/SearchPlaces';
import useFetchApiHomeHero from '../../hooks/HomeHeroHook/useFetchApiHomeHero';

export default function LandingPage() {
  const { homeHeroData } = useFetchApiHomeHero();

  return (
    <>
      <Header lightColor={true} />
      <BannerScreen
        lightColor={true}
        dataTitle={foodiesTitleBanner}
        subTitle1={foodiesSubTitleBanner.subTitle1}
        data={homeHeroData}
      />
      <WhoIsFoodies />
      <SearchPlaces />
      <SliderComponent />
      <FeedbackForm />
      <FoodiesApp />
      <Footer />
    </>
  );
}
