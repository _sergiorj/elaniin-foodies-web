import React, { useEffect, useState } from 'react';

export default function useFetchApiFooter() {
  const [footerData, setFooterData] = useState([]);

  async function fetchFooterData() {
    try {
      await fetch(`${import.meta.env.VITE_BASE_API_URL}/api/footers?populate=*`)
        .then((response) => response.json())
        .then((data) => {
          setFooterData(data.data);
        });
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    fetchFooterData();
  }, []);

  return {
    footerData,
  };
}
