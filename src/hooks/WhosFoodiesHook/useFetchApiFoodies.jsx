import React, { useEffect, useState } from 'react';

export default function useFetchApiFoodies() {
  const [foodiesData, setFoodiesData] = useState([]);

  async function fetchFoodiesData() {
    try {
      await fetch(
        `${
          import.meta.env.VITE_BASE_API_URL
        }/api/whos-foodies?populate=hero_img&populate=backgroung_img`
      )
        .then((response) => response.json())
        .then((data) => {
          setFoodiesData(data.data);
        });
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    fetchFoodiesData();
  }, []);

  return {
    foodiesData,
  };
}
