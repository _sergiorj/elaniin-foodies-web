import React, { useState, useEffect } from 'react';

export default function useFetchApiHomeHero() {
  const [homeHeroData, setHomeHeroData] = useState([]);

  async function fetchHomeHeroData() {
    try {
      await fetch(
        `${import.meta.env.VITE_BASE_API_URL}/api/home-heroes?populate=*`
      )
        .then((response) => response.json())
        .then((data) => {
          setHomeHeroData(data.data);
        });
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    fetchHomeHeroData();
  }, []);

  return {
    homeHeroData,
  };
}
