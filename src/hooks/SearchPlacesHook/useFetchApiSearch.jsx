import React, { useState, useEffect } from 'react';

export default function useFetchApiSearch({ searchParam, searchQuery }) {
  const [searchData, setSearchData] = useState([]);
  const [optionData, setOptionData] = useState([]);

  async function fetchSearchData() {
    try {
      await fetch(
        `${import.meta.env.VITE_BASE_API_URL}/api/search-places?populate=*`
      )
        .then((response) => response.json())
        .then((data) => {
          setSearchData(data.data);
        });
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    fetchSearchData();
  }, []);

  async function searchPlaceOption() {
    try {
      await fetch(
        `${
          import.meta.env.VITE_BASE_URL
        }locations?limit=3&type=${searchParam}&q=${searchQuery}`
      )
        .then((response) => response.json())
        .then((data) => {
          setOptionData(data.data);
        });
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    searchPlaceOption();
  }, [searchParam]);

  return {
    searchData,
    optionData,
    searchPlaceOption,
  };
}
