import React, { useEffect, useState } from 'react';

export default function useFetchApiSlider() {
  const [slideData, setSlideData] = useState([]);

  async function fetchSliderData() {
    try {
      await fetch(
        `${import.meta.env.VITE_BASE_API_URL}/api/testimonials?populate=*`
      )
        .then((response) => response.json())
        .then((data) => {
          setSlideData(data.data);
        });
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    fetchSliderData();
  }, []);

  return {
    slideData,
  };
}
