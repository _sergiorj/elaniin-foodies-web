import React, { useEffect, useState } from 'react';

export default function useFetchApiApp() {
  const [appData, setAppData] = useState([]);

  async function fetchAppData() {
    try {
      await fetch(`${import.meta.env.VITE_BASE_API_URL}/api/apps?populate=*`)
        .then((response) => response.json())
        .then((data) => {
          setAppData(data.data);
        });
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    fetchAppData();
  }, []);

  return {
    appData,
  };
}
