import React, { useEffect, useState } from 'react';

export default function useFetchApiMenu() {
  const [logoData, setLogoData] = useState([]);
  const [menuData, setMenuData] = useState([]);
  const [menuHeroData, setMenuHeroData] = useState([]);

  async function fetchLogoData() {
    try {
      await fetch(`${import.meta.env.VITE_BASE_API_URL}/api/logos`)
        .then((response) => response.json())
        .then((data) => {
          setLogoData(data.data);
        });
    } catch (error) {
      console.log(error);
    }
  }

  async function fetchMenuData() {
    try {
      await fetch(`${import.meta.env.VITE_BASE_API_URL}/api/menus`)
        .then((response) => response.json())
        .then((data) => {
          setMenuData(data.data);
        });
    } catch (error) {
      console.log(error);
    }
  }

  async function fetchMenuHeroData() {
    try {
      await fetch(
        `${import.meta.env.VITE_BASE_API_URL}/api/menu-heroes?populate=*`
      )
        .then((response) => response.json())
        .then((data) => {
          setMenuHeroData(data.data);
        });
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    fetchLogoData();
    fetchMenuData();
    fetchMenuHeroData();
  }, []);

  return {
    logoData,
    menuData,
    menuHeroData,
  };
}
