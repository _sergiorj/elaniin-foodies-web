import React, { useState, useEffect } from 'react';

export default function useFetchMenuCategories(pathFetch) {
  const [apiData, setApiData] = useState([]);
  const [categoriesC, setCategoriesC] = useState([
    {
      id: '',
      name: 'Todas',
      slug: 'todas',
    },
  ]);
  const [search, setSearch] = useState([]);

  const apiRequest = async () => {
    const options = {
      method: 'GET',
      headers: { 'Content-Type': 'application/json' },
    };

    await fetch(`${import.meta.env.VITE_BASE_URL}${pathFetch}`, options)
      .then((response) => response.json())
      .then((response) => {
        setApiData(response.data);
        setSearch(response.data);
        setCategoriesC([...categoriesC, ...response.data]);
      })

      .catch((err) => console.error(err));
  };

  useEffect(() => {
    apiRequest(pathFetch);
  }, [pathFetch]);

  return {
    apiData,
    categoriesC,
  };
}
