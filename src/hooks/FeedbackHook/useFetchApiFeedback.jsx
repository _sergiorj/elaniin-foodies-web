import React, { useEffect, useState } from 'react';

export default function useFetchApiFeedback() {
  const [feedbackData, setFeedbackData] = useState([]);
  async function fetchFeedbackData() {
    try {
      await fetch(
        `${import.meta.env.VITE_BASE_API_URL}/api/feedbacks?populate=*`
      )
        .then((response) => response.json())
        .then((data) => {
          setFeedbackData(data.data);
        });
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    fetchFeedbackData();
  }, []);

  return {
    feedbackData,
  };
}
