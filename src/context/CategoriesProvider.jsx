import React, { useState } from 'react';
import CategoriesContext from './CategoriesContext';

export default function CategoriesProvider({ children }) {
  const [categoryContext, setCategoryContext] = useState('001');
  const [searchContext, setSearchContext] = useState('dishes?page=1');
  const [allData, setAllData] = useState([]);

  return (
    <CategoriesContext.Provider
      value={{
        categoryContext,
        setCategoryContext,
        searchContext,
        setSearchContext,
        allData,
        setAllData,
      }}
    >
      {children}
    </CategoriesContext.Provider>
  );
}
