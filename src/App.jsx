import React from 'react';
import { Navigate, Route, Routes } from 'react-router-dom';
import CategoriesProvider from './context/CategoriesProvider';

import LandingPage from './screens/main/LandingPage';
import MenuScreen from './screens/menu/MenuScreen';

function App() {
  return (
    <CategoriesProvider>
      <section className="max-w-[1500px] m-auto bg-white">
        <Routes>
          <Route path="/" element={<LandingPage />} />
          <Route path="menu" element={<MenuScreen />} />
          <Route path="/*" element={<Navigate to="/" />} />
        </Routes>
      </section>
    </CategoriesProvider>
  );
}

export default App;
