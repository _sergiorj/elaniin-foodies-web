import React, { useState } from 'react';
import useFetchApiFeedback from '../../hooks/FeedbackHook/useFetchApiFeedback';
import FeedbackMessage from './FeedbackMessage';
import Form from './Form';

export default function FeedbackForm() {
  const [data, setData] = useState(true);

  const { feedbackData } = useFetchApiFeedback();

  return (
    <section id="feedback-container" role="article">
      {data ? (
        <Form setData={setData} data={data} feedbackData={feedbackData} />
      ) : (
        <FeedbackMessage
          setData={setData}
          data={data}
          feedbackData={feedbackData}
        />
      )}
    </section>
  );
}
