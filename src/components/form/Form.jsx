import React from 'react';

export default function Form({ data, setData, feedbackData }) {
  return (
    <>
      {feedbackData?.map((item) => (
        <section
          key={item.id}
          className="bg-black text-white flex flex-col w-full content-center justify-center items-center xl:h-[700px] lg:h-[700px] h-[868px]"
        >
          <section
            role="article"
            className="text-center mb-10 2xl:mt-28 xl:mt-28 lg:mt-28 -mt-10"
          >
            <h1 role="heading" className="font-druk font-bold text-4xl mb-4">
              {item.attributes.title}
            </h1>
            <p className="font-openSans m-auto font-normal text-2xl 2xl:w-2/3 xl:w-2/3 lg:w-2/3 md:w-2/4">
              {item.attributes.description}
            </p>
          </section>

          <section
            role="article"
            className="flex 2xl:flex-row xl:flex-row lg:flex-row flex-col w-3/4 md:mb-20 justify-center"
          >
            <section
              role="article"
              className="flex flex-col 2xl:mr-10 xl:mr-10 lg:mr-10 2xl:w-1/3 xl:w-1/3 lg:w-1/3 w-full"
            >
              <label className="flex flex-col font-sans text-xs text-white">
                {item.attributes.user_name}
                <input
                  id="name-input"
                  required
                  role="textbox"
                  type="text"
                  placeholder="John Doe"
                  className="bg-transparent font-sans text-base text-white border rounded-md h-[50px] 2xl:w-[300px] xl:w-[300px] lg:w-[290px] mt-1 p-3 w-full"
                />
              </label>
              <label className="flex flex-col font-sans text-xs text-white mt-3 md:w-full">
                {item.attributes.user_mail}
                <input
                  id="email-input"
                  required
                  role="textbox"
                  type="email"
                  placeholder="j.doe@correo.com"
                  className="bg-transparent font-sans text-base text-white border rounded-md h-[50px] 2xl:w-[300px] xl:w-[300px] lg:w-[290px] mt-1 p-3 w-full"
                />
              </label>
            </section>
            <section
              role="article"
              className="2xl:mt-0 xl:mt-0 lg:mt-0 md:mt-3"
            >
              <label className="flex flex-col font-sans text-xs text-white">
                {item.attributes.user_message}
                <textarea
                  id="message-input"
                  required
                  role="textbox"
                  type="text"
                  placeholder="El día de ahora mi experiencia fue..."
                  className="2xl:w-[590px] xl:w-[590px] lg:w-[490px] h-[132px] font-sans text-base text-white bg-transparent border rounded-md mt-1 p-3 w-full"
                />
              </label>
              <section className="w-full flex 2xl:justify-end xl:justify-end lg:justify-end right-0 mb-20 justify-center 2xl:mt-2 xl:mt-2 lg:mt-2 mt-12">
                <button
                  id="btn-form-submit"
                  role="article"
                  onClick={() => setData(!data)}
                  className="bg-yellow-400 p-3 rounded mt-6 text-black font-bold font-openSans text-base"
                >
                  {item.attributes.btn_title}
                </button>
              </section>
            </section>
          </section>
        </section>
      ))}
    </>
  );
}
