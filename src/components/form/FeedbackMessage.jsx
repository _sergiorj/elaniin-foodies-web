import React from 'react';

export default function FeedbackMessage({ data, setData, feedbackData }) {
  return (
    <>
      {feedbackData.map((item) => (
        <section
          key={item.id}
          className="bg-black text-white flex flex-col w-full content-center justify-center items-center xl:h-[700px] lg:h-[700px] h-[868px]"
        >
          <section role="article">
            <img
              id="letter-image-feedback"
              role="img"
              src={item.attributes.img_message.data.attributes.url}
              alt={item.attributes.img_message.data.attributes.alternativeText}
              className="w-[158px] h-[143px] mb-12"
            />
          </section>
          <section role="article" className="text-center text-white mb-8">
            <h1 role="heading" className="font-druk font-bold text-4xl mb-4">
              {item.attributes.btn_title_message}
            </h1>
            <p className="m-auto  font-openSans text-2xl w-2/3">
              {item.attributes.description_message}
            </p>
          </section>
          <section role="article">
            <button
              id="btn-form-back"
              role="button"
              onClick={() => setData(!data)}
              className="bg-yellow-400 p-4 rounded-md"
            >
              <span className="font-openSans font-bold text-base text-black">
                {item.attributes.btn_title_message}
              </span>
            </button>
          </section>
        </section>
      ))}
    </>
  );
}
