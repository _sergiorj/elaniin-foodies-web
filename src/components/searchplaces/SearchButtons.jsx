import React from 'react';

export default function SearchButtons({ item, searchParam, setSearchParam }) {
  return (
    <>
      <section className="flex flex-row">
        <button
          role="button"
          onClick={() => setSearchParam('takeout')}
          className={`w-1/2 p-5 ${
            searchParam === 'takeout'
              ? 'bg-black text-white'
              : 'bg-white text-black'
          }  flex 2xl:justify-center xl:justify-center lg:justify-center`}
        >
          <section className="2xl:p-1 xl:p-1 lg:p-1 p-1 mr-2">
            <img
              src={item.attributes.icon_btn_takeout.data.attributes.url}
              alt={
                item.attributes.icon_btn_takeout.data.attributes.alternativeText
              }
            />
          </section>
          <span className="font-syne font-bold text-2xl">
            {item.attributes.btn_title_takeout}
          </span>
        </button>
        <button
          role="button"
          onClick={() => setSearchParam('delivery')}
          className={`w-1/2 p-5 ${
            searchParam === 'delivery'
              ? 'bg-black text-white'
              : 'bg-white text-black'
          }  flex 2xl:justify-center xl:justify-center lg:justify-center`}
        >
          <section className="2xl:p-1 xl:p-1 lg:p-1 p-1 bg-white mr-2">
            <img
              src={item.attributes.icon_btn_delivery.data.attributes.url}
              alt={
                item.attributes.icon_btn_delivery.data.attributes
                  .alternativeText
              }
            />
          </section>
          <span className="font-syne font-bold text-2xl">
            {item.attributes.btn_title_delivery}
          </span>
        </button>
      </section>
    </>
  );
}
