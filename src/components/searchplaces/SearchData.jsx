import React from 'react';

export default function SearchData({ optionData }) {
  return (
    <>
      {optionData?.map((item) => (
        <section
          key={item.id}
          role="article"
          className="bg-white border border-black hover:bg-[#FFF1BF] hover:border-transparent p-3 mb-5
          2xl:w-[87%] xl:w-[87%] lg:w-[87%] rounded 2xl:mt-4 xl:mt-4 lg:mt-4 m-auto sm:mb-5"
        >
          <h1 role="heading" className="font-syne font-bold text-xl">
            {item.title}
          </h1>
          <p className="font-openSans text-base">
            {item.description.substring(0, 33)}
            <br />
            {item.description.substring(33, 110)}
          </p>
        </section>
      ))}
    </>
  );
}
