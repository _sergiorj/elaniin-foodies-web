import React, { useEffect, useState } from 'react';
import useFetchApiSearch from '../../hooks/SearchPlacesHook/useFetchApiSearch';
import SearchButtons from './SearchButtons';
import SearchData from './SearchData';

export default function SearchPlaces() {
  const [searchParam, setSearchParam] = useState('takeout');
  const [searchQuery, setSearchQuery] = useState('');

  const { searchData, optionData, searchPlaceOption } = useFetchApiSearch({
    searchParam,
    searchQuery,
  });

  return (
    <>
      {searchData.map((item) => (
        <section
          key={item.id}
          id="searchplaces-container"
          role="article"
          className="flex 2xl:flex-row 2xl:mt-20 2xl:mb-3 xl:flex-row xl:mt-20 xl:mb-3 lg:flex-row lg:mt-20 lg:mb-3 flex-col mt-20"
        >
          <section
            role="article"
            className="2xl:w-2/5 xl:w-2/5 lg:w-2/5 w-full"
          >
            <section className="w-full 2xl:text-center 2xl:mb-8 xl:text-center xl:mb-8 lg:text-center lg:mb-8 mb-8">
              <h1 role="heading" className="font-druk font-bold text-3xl">
                {item.attributes.title}
              </h1>
            </section>

            <SearchButtons
              item={item}
              searchParam={searchParam}
              setSearchParam={setSearchParam}
            />

            <section className="w-full border flex 2xl:flex-row xl:flex-row lg:flex-row 2xl:mb-0 xl:mb-0 lg:mb-0 mb-5">
              <button
                role="button"
                onClick={() => searchPlaceOption()}
                className="p-3 bg-white"
              >
                <img
                  src={item.attributes.icon_btn_search.data.attributes.url}
                  alt={
                    item.attributes.icon_btn_search.data.attributes
                      .alternativeText
                  }
                />
              </button>
              <input
                role="input"
                type="text"
                placeholder="Buscar nombre o dirección"
                className="w-full h-12"
                value={searchQuery}
                onChange={(e) => setSearchQuery(e.target.value)}
                onKeyDown={(e) => {
                  if (e.key === 'Enter') {
                    searchPlaceOption();
                  }
                }}
              />
            </section>
            <section
              id="search-results-container"
              role="article"
              className="mb-5"
            >
              <SearchData optionData={optionData} />
            </section>
          </section>
          <section role="article" className="2xl:w-3/5 xl:w-3/5 lg:w-3/5 ">
            <img
              id="map-image-container"
              role="img"
              src={item.attributes.hero_img.data.attributes.url}
              alt={item.attributes.hero_img.data.attributes.alternativeText}
              className="w-full"
            />
          </section>
        </section>
      ))}
    </>
  );
}
