import React from 'react';

import useFetchApiApp from '../../hooks/AppHook/useFetchApiApp';

export default function FoodiesApp() {
  const { appData } = useFetchApiApp();

  return (
    <section id="foodies-app-container" role="article">
      {appData.map((item) => (
        <section
          key={item.id}
          className="flex flex-row mb-10 justify-between relative 2xl:h-[870px] xl:h-[770px] lg:w-[70%] md:flex-col"
        >
          <section
            role="article"
            className="absolute h-[750px] m-auto md:w-full"
          >
            <img
              id="foodies-app-vector"
              role="img"
              src={item.attributes.img_hero.data.attributes.url}
              alt="Foodies app"
              className="2xl:-mt-28 2xl:flex xl:flex xl:-mt-28 lg:flex 2xl:h-[900px] xl:h-[900px] lg:h-[900px] lg:-mt-28 lg:-ml-5 md:flex md:-mt-32 md:mr-20 md:mb-20 md:h-[720px] md:m-auto sm:hidden hidden"
            />
          </section>

          <section
            role="article"
            className="2xl:w-[665px] xl:w-[665px] 2xl:mt-20 xl:mt-20 2xl:ml-[570px] xl:ml-[570px] lg:ml-[60%] lg:w-[70%] lg:mt-12 md:mt-96 w-full relative"
          >
            <section
              id="foodies-heading-container"
              role="article"
              className="text-center"
            >
              <h1
                role="heading"
                className="font-druk m-auto font-bold 2xl:w-full xl:w-full lg:w-full w-full text-3xl mt-14 text-black 2xl:text-4xl xl:text-4xl lg:text-3xl md:text-4xl md:mt-20 md:text-center md:w-2/3"
              >
                {item.attributes.title_part_one}
                <section className="bg-[#FFD600] -rotate-1">
                  <span className="rotate-1">
                    {item.attributes.title_part_two}
                  </span>
                </section>
              </h1>
            </section>

            <section
              role="article"
              className="flex flex-col justify-center 2xl:w-[665px] xl:w-[665px] lg:w-[600px] mb-12 relative"
            >
              <img
                id="foodies-app-vector-2"
                role="img"
                src={item.attributes.img_background.data.attributes.url}
                alt="Vector lines"
                className="absolute 2xl:top-0 2xl:flex xl:flex xl:top-0 2xl:left-20 xl:left-20 2xl:mt-10 xl:mt-10 lg:flex lg:-mt-10 lg:ml-0 md:hidden sm:hidden hidden"
              />
              <section
                id="foodies-app-steps"
                role="article"
                className="flex 2xl:flex-row xl:flex-row lg:flex-row md:flex-row flex-col 2xl:mt-7 xl:mt-7 xl:ml-20 justify-between mt-20 m-auto md:w-full"
              >
                <section className="text-center flex flex-col w-72 2xl:-ml-10 xl:-mt-10 2xl:mr-10 xl:mr-10 lg:-ml-14 lg:mt-0 md:ml-48 md:-mb-10 sm:mb-20 mb-20 ">
                  <span className="bg-yellow-400 p-2 w-10 mb-4 text-white font-bold rounded-3xl m-auto">
                    {item.attributes.point_one}
                  </span>
                  <h1 className="font-sans font-bold text-[22px] mb-4">
                    {item.attributes.subTitle_one}
                  </h1>
                  <span>{item.attributes.description_one}</span>
                </section>
                <section className="text-center flex flex-col 2xl:mr-24 xl:mr-24 w-72 lg:mr-5 md:mr-48 md:-mb-10">
                  <span className="bg-yellow-400 p-2 w-10 mb-4 text-white font-bold rounded-3xl m-auto">
                    {item.attributes.point_two}
                  </span>
                  <h1 className="font-sans font-bold text-[22px] mb-4">
                    {item.attributes.subTitle_two}
                  </h1>
                  <span>{item.attributes.description_two}</span>
                </section>
              </section>
              <section
                id="foodies-app-steps-2"
                role="article"
                className="text-center flex flex-col mt-16 2xl:ml-[177px] w-72 xl:ml-44 lg:ml-24 md:m-auto md:mt-24 md: m-auto"
              >
                <span className="bg-yellow-400 p-2 w-10 mb-4 text-white font-bold rounded-3xl m-auto">
                  {item.attributes.point_three}
                </span>
                <h1 className="font-sans font-bold text-[22px] mb-4">
                  {item.attributes.subTitle_three}
                </h1>
                <span>{item.attributes.description_three}</span>
              </section>
            </section>
          </section>
        </section>
      ))}
    </section>
  );
}
