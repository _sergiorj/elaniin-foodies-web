import React from 'react';
import BtnWithArrow from '../btns/BtnWithArrow';
import useFetchApiFoodies from '../../hooks/WhosFoodiesHook/useFetchApiFoodies';

export default function WhoIsFoodies() {
  const { foodiesData } = useFetchApiFoodies();

  return (
    <section role="article" id="who-is-foodies-section">
      {foodiesData?.map((foodies) => (
        <section
          key={foodies.id}
          role="article"
          className="flex flex-col
      2xl:grid 2xl:grid-cols-2 2xl:mt-[80.6px] 2xl:h-[565px] xl:grid xl:grid-cols-2 xl:mt-[80.6px] xl:h-[565px] lg:flex lg:flex-row
      md:flex md:flex-col sm:flex sm:flex-col"
        >
          <section className="relative xl:h-[565px] xl:w-[816px] xl:-ml-[135px] lg:h-[100%] lg:justify-center lg:items-center lg:bg-indigo-400 md:w-[100%] md:jusify-center md:items-center">
            <img
              id="who-is-foodies-hero-img"
              role="img"
              src={foodies.attributes.hero_img.data.attributes.url}
              alt="Who is foodies"
              className="object-contain md:w-full md:justify-center md:items-center sm:w-full sm:justify-center sm:items-center"
            />
            <h1
              role="heading"
              className="font-druk font-normal text-end pr-10 text-white 2xl:hidden xl:hidden lg:hidden md:-mt-40 md:pr-52 md:text-5xl
        sm:-mt-36 sm:text-5xl -mt-48 text-5xl"
            >
              {foodies.attributes.subtitle_one} <br />
              <span className="text-yellow-400">
                {foodies.attributes.subtitle_two}
              </span>
            </h1>
          </section>
          <section
            role="article"
            className="text-left xl:ml-[73px] xl:h-[565px] xl:w-[685px] lg: w-full lg:-mt-1 md:mt-12 sm:mt-20 mt-20 mb-7"
          >
            <img
              id="who-is-foodies-background-img"
              role="img"
              src={foodies.attributes.backgroung_img.data.attributes.url}
              className="2xl:flex xl:flex xl:h-[565px] lg:flex lg:h-[400px] object-none md:flex md:w-full hidden"
            />
            <section
              role="article"
              className="xl:px-14 xl:-mt-[420px] lg:-mt-[320px] lg:w-full md:-mt-80 md:px-24 sm:px-10 sm:mb-7"
            >
              <h3 className="font-syne font-bold text-[22px] md:mb-10 sm:mb-7 mb-7">
                {foodies.attributes.title}
              </h3>
              <p className="font-openSans font-normal text-black/[0.4] text-[18px] xl:mt-[27px] xl:mb-[54px] lg:mb-14 md:mb-14 sm:mb-7 mb-7">
                {foodies.attributes.description}
              </p>
              <BtnWithArrow titleTlex={foodies.attributes.btn_title} />
            </section>
          </section>
        </section>
      ))}
    </section>
  );
}
