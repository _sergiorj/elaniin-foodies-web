import React, { useState, useContext } from 'react';
import CategoriesContext from '../../context/CategoriesContext';
import useFetchMenuCategories from '../../hooks/MenuHook/useFetchMenuCategories';
import HamburguerCloseMenu from '../icons/HamburguerCloseMenu';
import SearchIncon from '../icons/SearchIncon';
import SettingsMenu from '../icons/SettingsMenu';

export default function FoodCategorySearch() {
  const { categoriesC } = useFetchMenuCategories('categories');
  const [setTexto, setSetTexto] = useState('');
  const [isButtonActive, setIsButtonActive] = useState(false);

  const { categoryContext, setCategoryContext, setSearchContext } =
    useContext(CategoriesContext);

  function handleSelectCategory(category) {
    setCategoryContext(category);
    if (category === 'todas') {
      setSearchContext('dishes?page=1');
    } else {
      const params = {
        categories: [category],
      };
      const jsonParam = Object.keys(params)
        .map((key) => {
          if (key === 'categories') {
            return params[key]
              .map((item) => {
                if (item === '') return;
                return `categories[]=${item}`;
              })
              .join('&');
          }
          return `&${key}=${params[key]}`;
        })
        .join('&');
      console.log(jsonParam);

      setSearchContext(`dishes?${jsonParam}`);
    }
  }

  function handleSearchFood(event) {
    setSetTexto(event.target.value);
  }

  function handleSearchFoodSubmit(event) {
    event.preventDefault();
    setSearchContext(`dishes?q=${setTexto}`);
  }

  return (
    <section
      role="article"
      className="flex mb-10 w-full content-center justify-center items-center 2xl:flex-row 2xl:mb-14 xl:flex-row xl:mb-14 
      lg:flex-row lg:mb-14 md:flex-col md:mb-[70px] md:justify-items-center sm:flex sm:w-full sm:justify-center"
    >
      <section
        role="article"
        className="w-[467px] h-12 2xl:ml-16 2xl:mr-11 2xl:w-[467px] xl:ml-16 xl:mr-11 xl:w-[467px] lg:ml-7 lg:mr-6 lg:w-80 
        md:flex md:w-full md:justify-center sm:flex sm:flex-row"
      >
        <section
          role="article"
          className="flex flex-row w-[467px] h-12 border border-[#CBD5E0] rounded-lg 2xl:w-[467px] 2xl:h-12
          xl:w-[467px] xl:h-12 lg:w-[467px] lg:h-12 md:w-[467px] md:content-center md:justify-ceenter md:items-center"
        >
          <button
            id="searchButton-icon"
            role="button"
            onClick={(e) => handleSearchFoodSubmit(e)}
            className="w-6 h-6 ml-[15px] mt-[13px] mb-[13px] mr-[24px]"
          >
            <SearchIncon />
          </button>
          <input
            id="searchInput-text"
            role="input"
            type="text"
            className="w-full mr-2 bg-[#F8F8F8]"
            placeholder="Busca tu platillo favorito..."
            value={setTexto}
            onChange={handleSearchFood}
            onKeyDown={(e) => {
              if (e.key === 'Enter') {
                handleSearchFoodSubmit(e);
              }
            }}
          />
        </section>
      </section>
      <section
        role="button"
        onClick={() => setIsButtonActive(!isButtonActive)}
        className="z-50 flex ml-5 p-3 2xl:hidden xl:hidden lg:hidden md:hidden"
      >
        {isButtonActive ? (
          <HamburguerCloseMenu className="z-50" />
        ) : (
          <SettingsMenu className="z-50" />
        )}
      </section>
      <section
        id="categories-container"
        role="article"
        className={`z-50 w-full text-start font-syne font-bold text-lg hidden relative 2xl:flex 2xl:mt-0 2xl:text-start 
        xl:flex xl:mt-0 xl:text-start lg:flex lg:mt-0 lg:text-start md:flex md:text-center md:justify-center md:mt-7 
       ${
         isButtonActive
           ? 'sm:flex sm:bg-black sm:text-white sm:p-5 sm:flex-col sm:absolute sm:w-full sm:space-between sm:mt-[275px] sm:text-start'
           : ''
       }`}
      >
        {categoriesC.map((category) => (
          <section role="listitem" className="relative mr-10" key={category.id}>
            <button
              id="category-button"
              role="button"
              onClick={() => handleSelectCategory(category.id)}
            >
              <span className={`relative z-50 sm:mb-5`}>{category.name}</span>
              <section
                className={`${
                  category.id === categoryContext &&
                  'bg-[#FFD600] h-[6px] w-full absolute -my-[15px] justify-center'
                }`}
              />
            </button>
          </section>
        ))}
      </section>
    </section>
  );
}
