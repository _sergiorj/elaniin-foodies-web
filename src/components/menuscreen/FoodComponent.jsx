import React, { useContext, useEffect } from 'react';
import currencyFormatter from 'currency-formatter';
import useFetchMenuCategories from '../../hooks/MenuHook/useFetchMenuCategories';
import CategoriesContext from '../../context/CategoriesContext';
import NoResults from '../error/NoResults';

export default function FoodComponent() {
  const { searchContext, setAllData } = useContext(CategoriesContext);
  const { apiData } = useFetchMenuCategories(searchContext);

  useEffect(() => {
    setAllData(apiData);
  }, [apiData]);

  if (apiData?.length <= 0) {
    return (
      <section className="container m-auto pt-20 pb-20">
        <NoResults />
      </section>
    );
  }

  return (
    <section
      id="food-component-container"
      role="article"
      className="grid grid-cols-1 justify-items-center mb-[30px] xl:grid-cols-4 lg:grid-cols-3 md:grid-cols-2 sm:grid-cols-1"
    >
      {apiData?.map((food) => (
        <section
          id="food-component"
          role="article"
          key={food.id}
          className="w-[330px] h-[499px] mb-[20px] flex-col transform transition duration-300 group hover:scale-105 hover:bg-white hover:h-[499px] hover:drop-shadow-md hover:rounded-lg"
        >
          <img
            role="img"
            className="w-[330px] h-[264px] object-cover bg-yellow-300 rounded-lg"
            src={food.image}
            alt="Menu foodies"
          />
          <section role="article" className="h-[240px]">
            <h4 className="text-start font-syne font-bold text-[22px] p-[10px]">
              {food.title}
            </h4>
            <p className="text-start font-sans font-normal text-lg p-[10px] h-[125px]">
              {food.description.substring(0, 107).concat('...')}
            </p>
            <section
              role="article"
              className="flex justify-between px-[10px] py-[10px]"
            >
              <span className="font-sans font-normal text-lg text-[#78909C]">
                {food.categories.length > 0
                  ? food.categories[0].name
                  : 'Descúbrelo!'}
              </span>
              <section className="bg-[#FFD600] px-[10px] py-[6px] rounded-[8px]">
                <span className="text-center font-druk font-bold text-base">
                  {currencyFormatter.format(food.price / 100, {
                    code: 'USD',
                  })}
                </span>
              </section>
            </section>
          </section>
        </section>
      ))}
    </section>
  );
}
