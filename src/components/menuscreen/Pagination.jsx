import React from 'react';

export default function Pagination() {
  const pageList = [
    {
      id: 1,
      active: true,
    },
  ];

  return (
    <section
      role="article"
      className="flex flex-col w-full justify-center items-center mb-12
      2xl:flex-row 
      xl:flex-row  
      lg:flex-row 
      md:flex-col 
      sm:flex-col
    "
    >
      <section className="mb-4">
        {pageList.map((item) => {
          return (
            <button
              role="button"
              className={`w-[48px] h-[47px] mr-[10px] border rounded-[8px] ${
                item.active
                  ? 'bg-black text-[#FFD600]'
                  : 'border-[#C4C4C4]  bg-[#F8F8F8] text-black'
              }`}
              key={item.id}
            >
              {item.id}
            </button>
          );
        })}
      </section>
    </section>
  );
}
