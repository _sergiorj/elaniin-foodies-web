import React from 'react';
import FoodComponent from './FoodComponent';

export default function FoodList() {
  return (
    <section role="article">
      <FoodComponent />
    </section>
  );
}
