import React from 'react';
import imgPromo from '../../assets/foodies/main-img-letter.png';
import BtnWithArrow from '../buttons/BtnWithArrow';
import vectorFoodies from '../../assets/foodies/vector-foodie.png';

export default function WhoIsFoodies() {
  return (
    <div className="grid grid-cols-2 xl:mt-[80.6px] xl:h-[565px">
      <section className="xl:h-[565px] xl:w-[816px] xl:-ml-[135px]">
        <img src={imgPromo} alt="Imagen" className="object-contain" />
      </section>
      <section className="text-left xl:ml-[73px] xl:h-[565px] xl:w-[685px]">
        <img src={vectorFoodies} className="xl:h-[565px] object-none" />
        <section className="xl:px-14 xl:-mt-[420px] ">
          <h3 className="font-syne font-bold text-[22px]">
            ¿Quién es Foodies?
          </h3>
          <p className="font-openSans font-normal text-black/[0.4] text-[18px] xl:mt-[27px] xl:mb-[54px]">
            Elit irure ad nulla id elit laborum nostrud mollit irure. Velit
            reprehenderit sunt nulla enim aliquip duis tempor est culpa fugiat
            consequat culpa consectetur Lorem. Reprehenderit dolore culpa irure
            eiusmod minim occaecat et id minim ullamco.
          </p>
          <BtnWithArrow titleTlex="Contáctanos" />
        </section>
      </section>
    </div>
  );
}
