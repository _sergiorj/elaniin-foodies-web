import React from 'react';
import RightArrow from '../icons/RightArrow';

export default function BtnWithArrow({ titleTlex = 'Encuentranos' }) {
  return (
    <div className="flex flex-row -mt-1">
      <span className="text-black mr-1 font-syne font-bold text-[22px]">
        {titleTlex}
      </span>
      <RightArrow />
    </div>
  );
}
