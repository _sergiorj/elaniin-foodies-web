import React from 'react';

export default function HamburguerMenu() {
  return (
    <svg
      width="26"
      height="26"
      viewBox="0 0 26 26"
      fill="white"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M3.25 13H22.75"
        stroke="white"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M3.25 6.5H22.75"
        stroke="white"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M3.25 19.5H22.75"
        stroke="white"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
}
