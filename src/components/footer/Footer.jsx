import React from 'react';
import { Link } from 'react-router-dom';
import useFetchApiFooter from '../../hooks/FooterHook/useFetchApiFooter';

export default function Footer() {
  const { footerData } = useFetchApiFooter();

  return (
    <footer id="footer-container">
      {footerData?.map((item) => (
        <section key={item.id} role="article" className="pb-12 px-24">
          <section className="grid grid-cols-6 sm:justify-items-center">
            <section
              id="foodies-logo"
              role="article"
              className="col-start-1 col-end-3 2xl:-ml-36 xl:-ml-36 lg:-ml-36 font-druk font-normal text-[27px]"
            >
              <Link to={item.attributes.logo_link}>
                {item.attributes.logo_name}
              </Link>
            </section>
            <section className="col-end-7 col-span-2 mb-[29px]">
              <section id="btns-footer-containers" className="grid grid-col-2 ">
                <img
                  role="img"
                  src={item.attributes.img_store_google.data.attributes.url}
                  alt={
                    item.attributes.img_store_google.data.attributes
                      .alternativeText
                  }
                  className="col-start-1 xl:w-[184px] xl:h-[54px] lg:w-[184px] lg:h-[54px] lg:mr-[10px]
              md:w-[133px] md:h-[39px] sm:w-[238px] sm:h-[34px]"
                />
                <img
                  role="img"
                  src={item.attributes.img_store_apple.data.attributes.url}
                  alt={
                    item.attributes.img_store_apple.data.attributes
                      .alternativeText
                  }
                  className="col-start-2 xl:w-[184px] xl:h-[54px] lg:w-[184px] lg:h-[54px]
              md:w-[133px] md:h-[39px] sm:w-[238px] sm:h-[34px]"
                />
              </section>
            </section>
          </section>
          <hr className=" border-b-[1px] w-full border-[#FFC700]" />
          <section
            id="footer-routes-container"
            role="article"
            className="flex flex-col text-center w-full mt-[23px] font-sans font-normal text-base text-black/[0.4]
        2xl:flex 2xl:flex-row 2xl:justify-evenly xl:flex xl:flex-row xl:justify-evenly lg:flex lg:flex-row lg:justify-evenly
        md:grid md:grid-row-5 md:grid-flow-col sm:flex sm:text-center sm:flex-col"
          >
            <span>{item.attributes.menu_op1}</span>
            <span>{item.attributes.menu_op2}</span>
            <span>{item.attributes.menu_op3}</span>
            <span>{item.attributes.menu_op4}</span>
            <span>{item.attributes.menu_op5}</span>
          </section>
        </section>
      ))}
    </footer>
  );
}
