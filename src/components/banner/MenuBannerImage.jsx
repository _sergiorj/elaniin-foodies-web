import React from 'react';

export default function MenuBannerImage({ menuHeroData }) {
  return (
    <>
      {menuHeroData?.map((item) => (
        <img
          key={item.id}
          className="z-0 bg-no-repeat bg-cover w-[100%] h-[395px] 
            2xl:w-[42%]
            xl:w-[48.8%] 
            lg:w-[49%] lg:h-[610px]
            md:w-[50%] md:h-[504px]
            sm:w-[100%] sm:flex-end sm:h-[495px]"
          src={item.attributes.hero_img.data.attributes.url}
        />
      ))}
    </>
  );
}
