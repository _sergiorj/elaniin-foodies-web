import React from 'react';

export default function LandingBannerImage({ homeHeroData }) {
  return (
    <>
      {homeHeroData?.map((item) => (
        <img
          key={item.id}
          className={`z-0 w-[100%] h-[593px] 2xl:w-[42%] xl:w-[48.8%] lg:w-[49%] lg:h-[793px] md:w-[50%]  md:h-[589px]
            sm:w-[100%] sm:flex sm:h-[593px]`}
          src={item.attributes.hero_img.data.attributes.url}
        />
      ))}
    </>
  );
}
