export const foodiesTitleBanner = {
  fontColorTitle: 'text-black',
  fontColorMarker: 'text-black',
};
export const foodiesSubTitleBanner = {
  subTitle1: 'Estamos a punto de descubrir un mundo lleno de sabores',
  subTitle2: 'y de emociones inigualables.',
};

export const menuTitleBanner = {
  title1: 'Cada sabor',
  title2: 'es una nueva',
  title3: 'experiencia',
  fontColorTitle: 'text-white',
  fontColorMarker: 'text-black',
};
