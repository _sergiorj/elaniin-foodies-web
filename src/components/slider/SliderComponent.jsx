import React from 'react';
import useFetchApiSlider from '../../hooks/HomeSliderComponent/useFetchApiSlider';

export default function SliderComponent() {
  const { slideData } = useFetchApiSlider();

  return (
    <>
      {slideData?.map((slides) => (
        <section
          key={slides.id}
          id="testimonials-container"
          role="slider"
          className="flex flex-row justify-between h-[600px] mt-5 relative"
        >
          <section role="article">
            <img
              id="vector-a-slider"
              role="img"
              src={slides.attributes.img_vector_left.data.attributes.url}
              alt="Ketchup shape"
              className="hidden 2xl:flex xl:flex lg:flex md:flex"
            />
          </section>
          <section
            role="article"
            className="text-center mx-auto my-auto 2xl:w-2/4 xl:w-2/4 lg:w-2/4"
          >
            <section className="flex flex-col">
              <h1
                className="font-druk font-bold text-black mb-5
            2xl:text-4xl xl:text-4xl lg:text-4xl md:text-3xl text-xl"
              >
                {slides.attributes.title}
              </h1>
              <p className="font-openSans text-black/[0.4] w-2/3 m-auto mb-14">
                {slides.attributes.description}
              </p>
            </section>
            <section role="group" className="flex items-center justify-center">
              <img
                role="img"
                src={
                  slides.attributes.img_pagination_indicator.data.attributes.url
                }
                alt="Pagination"
              />
            </section>
          </section>
          <section role="article">
            <img
              id="vector-c-slider"
              src={slides.attributes.img_vector_bottle.data.attributes.url}
              alt="Ketchup bottle"
              className="absolute hidden 2xl:-ml-[250px] 2xl:flex xl:-ml-[250px] xl:flex lg:-ml-[250px] lg:flex"
            />
            <img
              id="vector-b-slider"
              role="img"
              src={slides.attributes.img_vector_right.data.attributes.url}
              alt="Ketchup shape"
              className="mt-28 hidden 2xl:flex xl:flex lg:flex md:flex md:h-dull md:w-full"
            />
          </section>
        </section>
      ))}
    </>
  );
}
