import React from 'react';
import noHamburguer from '../../assets/error/404-hamburguer.png';

export default function NoResults() {
  return (
    <section role="article" className="">
      <section role="article">
        <img
          role="img"
          src={noHamburguer}
          alt="Thanks for your message"
          className="w-[168px] h-[187px] mb-12 m-auto"
        />
      </section>
      <section role="article" className="text-center text-black mb-8">
        <h1 className="font-syne font-bold text-xl mb-4">
          ¡Platillo no encontrado!
        </h1>
        <p className="m-auto  font-openSans text-lg w-2/3">
          Te invitamos a que verifiques si el nombre esta bien escrito o prueba
          buscando un nuevo platillo.
        </p>
      </section>
    </section>
  );
}
