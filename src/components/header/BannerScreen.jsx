import React from 'react';
import PropTypes from 'prop-types';

import BtnWithArrow from '../btns/BtnWithArrow';
import LandingBannerImage from '../banner/LandingBannerImage';
import MenuBannerImage from '../banner/MenuBannerImage';

export default function BannerScreen({
  lightColor,
  dataTitle,
  subTitle1,
  data,
}) {
  return (
    <section
      id="banner-text-container"
      role="article"
      className={`z-0 ${
        lightColor ? 'bg-white' : 'bg-black'
      } flex flex-col-reverse mb-[80.96px]
        2xl:flex-row 2xl:-mt-[150px] xl:flex-row xl:-mt-[150px] lg:flex-row lg:-mt-[150px]
        md:flex-row md:-mt-[110px] sm:flex-col-reverse sm:-mt-[125px] -mt-[125px]`}
    >
      {data?.map((item) => (
        <section
          key={item.id}
          role="article"
          className="z-0 flex flex-1 flex-col items-start justify-center text-left"
        >
          <h1
            className={`z-0 mt-[10%] mb-[20%] ml-[10%] leading-none tracking-wide ${dataTitle.fontColorTitle} font-druk font-bold text-[38px]
          2xl:text-[60px] 2xl:mt-28 2xl:ml-[15%] xl:text-[60px] xl:mt-28 xl:ml-[10%] lg:text-[60px] lg:mt-28 lg:ml-[5%]   
          md:text-[40px] md:mt-28 md:ml-[10%] text-6xl sm:mt-[10%] `}
          >
            {item.attributes.title_one}
            <br />
            {item.attributes.title_two}
            <br />
            <section
              className={`bg-[#FFD600] -rotate-1 ${
                lightColor ? 'w-[80%]' : 'w-[100%]'
              } -ml-5`}
            >
              <span
                className={`-mr-5 rotate-1 ${dataTitle.fontColorMarker}  inline-block px-4`}
              >
                {item.attributes.title_three}
              </span>
            </section>
          </h1>
          {subTitle1 && (
            <section className="2xl:ml-[15%] 2xl:-mt-20 xl:ml-[10%] xl:-mt-10 lg:ml-[5%] lg:-mt-5 md:ml-[10%] md:-mt-10 ml-[10%] -mt-10">
              <p
                className="font-openSans font-normal mt-5 mb-12  text-black/[0.4] 
            2xl:text-[18px] xl:text-[18px] lg:text-[18px] md:text-[16px] text-xl"
              >
                {item.attributes.description}
              </p>
              <BtnWithArrow
                role="button"
                id="btn-find-us"
                titleTlex={item.attributes.btn_title}
              />
            </section>
          )}
        </section>
      ))}

      {lightColor ? (
        <LandingBannerImage homeHeroData={data} />
      ) : (
        <MenuBannerImage menuHeroData={data} />
      )}
    </section>
  );
}

BannerScreen.propTypes = {
  lightColor: PropTypes.bool.isRequired,
  dataTitle: PropTypes.objectOf(PropTypes.string).isRequired,
  subTitle1: PropTypes.string,
  subTitle2: PropTypes.string,
};
