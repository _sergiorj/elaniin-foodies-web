import React from 'react';
import hamburguerImg from '../../assets/header/hero_hamburger.png';
import BtnWithArrow from '../buttons/BtnWithArrow';

export default function LandingPromo() {
  return (
    <header className="z-0 -mt-[77px]">
      <section className="z-0 flex flex-row">
        <section className="w-full p-10 text-left lg:-mr-20 lg:-ml-20 lg:mt-40">
          <h1 className="mt-10 tracking-wide font-druk font-bold text-[60px] md:w-10/12 text-6xl lg:w-full  ">
            Un nuevo
            <br />
            sabor está en
            <br />
            <span className="bg-[#FFD600] xl:-ml-5 text-black -rotate-1 inline-block px-4">
              la ciudad
            </span>
          </h1>
          <p className="font-openSans font-normal mt-5 mb-12  text-black/[0.4] xl:text-lg">
            Estamos a punto de descubrir un mundo lleno de sabores
            <br />y de emociones inigualables.
          </p>
          <BtnWithArrow titleTlex="Encuentranos" />
        </section>
        <section className="z-0 xl:-mt-8  xl:w-10/12 xl:-ml-60 xl:-mr-[150px]">
          <img src={hamburguerImg} alt="Hamburguer-logo" className="z-0" />
        </section>
      </section>
    </header>
  );
}
