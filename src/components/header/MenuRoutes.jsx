import React from 'react';
import { Link } from 'react-router-dom';
import useFetchApiMenu from '../../hooks/MenuHook/useFetchApiMenu';

export default function MenuRoutes({ lightColor }) {
  const { menuData } = useFetchApiMenu();

  return (
    <>
      {menuData?.map((menu) => (
        <li
          key={menu.id}
          className={`z-50 ml-[17px] mb-[50px] ${
            lightColor ? 'text-black' : 'text-white'
          } font-syne font-bold text-[18px]
              2xl:ml-0 2xl:mb-0
              lg:mr-[39px] lg:ml-0 lg:mb-0
              md:ml-[17px] md:mb-[50px]
              sm:ml-[17px] sm:mb-[50px]`}
        >
          <Link
            to={menu.attributes.link}
            className="hover:text-orange-600 duration-500"
          >
            {menu.attributes.name}
          </Link>
        </li>
      ))}
    </>
  );
}
