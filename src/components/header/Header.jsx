import React, { useState } from 'react';
import PropTypes from 'prop-types';

import HamburguerCloseMenu from '../icons/HamburguerCloseMenu';
import HamburguerMenu from '../icons/HamburguerMenu';
import MenuRoutes from './MenuRoutes';
import { Link } from 'react-router-dom';
import useFetchApiMenu from '../../hooks/MenuHook/useFetchApiMenu';

export default function Header({ lightColor }) {
  const [isButtonActive, setIsButtonActive] = useState(false);
  const { logoData } = useFetchApiMenu();

  return (
    <header className={`z-50 ${lightColor ? 'bg-white' : 'bg-black'}`}>
      <nav className="z-50 2xl:flex xl:flex lg:flex py-4">
        <section className="z-50 flex flex-row justify-between">
          {logoData?.map((logo) => (
            <span
              id="foodies-logo"
              key={logo.id}
              role="button"
              className={`z-50 font-bold text-[27px] cursor-pointer ml-4 mt-12 font-druk ${
                lightColor ? 'text-black' : 'text-white'
              }
          2xl:ml-24 2xl:mt-14 2xl:mr-20 xl:ml-24 xl:mt-14 xl:mr-20 lg:ml-24 lg:mt-14 lg:mr-20
          md:ml-10 md:mt-7 sm:ml-4 sm:mt-12`}
            >
              <Link to={logo.attributes.link}>{logo.attributes.name}</Link>
            </span>
          ))}
          <section
            onClick={() => setIsButtonActive(!isButtonActive)}
            className="z-50 mr-[17px]
            2xl:hidden 
            xl:hidden 
            lg:hidden"
          >
            {isButtonActive ? (
              <HamburguerCloseMenu className="z-50" />
            ) : (
              <HamburguerMenu className="z-50" />
            )}
          </section>
        </section>
        <ul
          className={`z-50 text-start pt-[50px] pb-[20px] ${
            lightColor ? 'bg-white' : 'bg-black'
          } drop-shadow-xl mb-[100px]
           2xl:flex 2xl:mt-14 2xl:pt-0 2xl:pb-0 2xl:drop-shadow-none 2xl:bg-transparent 2xl:mb-0
           xl:flex xl:mt-14  xl:pb-1 xl:drop-shadow-none xl:bg-transparent xl:mb-0
           lg:flex lg:mt-14 lg:pt-1 lg:ml-[86px] lg:pb-0 lg:drop-shadow-none lg:bg-transparent lg:mb-0
           md:w-full md:mt-7  md:text-start  md:rounded-b-lg md:drop-shadow-xl 
           sm:text-start sm:rounded-b-lg sm:drop-shadow-xl ${
             isButtonActive ? '' : 'hidden'
           }`}
        >
          <MenuRoutes lightColor={lightColor} />
        </ul>
      </nav>
    </header>
  );
}

Header.propTypes = {
  lightColor: PropTypes.bool.isRequired,
};
