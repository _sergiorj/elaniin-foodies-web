# Foodies

Version 🏷️ 2.0.0

![Image](promo.png)

Foodies is a restaurant that doesn’t have dine-in locations, so customers need to order by calling. The
calls tend to be long, especially with new customers, because the menu has to be explained every time.

## Getting Started

- First, add node packages 📦.

```bash
yarn
# or
npm install
```

- Second, create .env file in root `/.env` and add the urls variables 🔐.
- `VITE_BASE_API_URL=` is the url that dev foodies-cms provide ex: `http://localhost:1337`

```bash
VITE_BASE_URL=

VITE_BASE_API_URL=
```

- Third, run the development server 💻.

```bash
yarn dev
# or
npm run dev
```

Open [http://127.0.0.1:5173/](http://127.0.0.1:5173/) with your browser to see the result.

## Testing 🧪

- First, it should be running the project in local host [http://127.0.0.1:5173/], follow the steps above to do it.

- Second, run the tests you want to try.

```bash
## Jest Tests

yarn test
# or
npm test

## Cypress Tests

yarn cypress:run
# or
npm cypress:run
```
