describe('Menu page tests', () => {
  beforeEach(() => {
    cy.visit('/menu');
    cy.viewport(1200, 750);
  });

  // it('Should show the Menu page components.', () => {
  //   cy.get('#foodies-logo > a').should('exist').and('be.visible');

  //   cy.get('#banner-text-container').then((elements) => {
  //     const bannerText = elements.find('h1').text();
  //     expect(bannerText).to.exist;
  //   });
  // });

  // it('Should render <FoodCategorySearch />.', () => {
  //   cy.get('#searchInput-text').should('exist').and('be.visible').type('peque');
  //   cy.get('#searchInput-text').should('have.value', 'peque');
  //   cy.get('#searchButton-icon').should('exist').and('be.visible').click();
  // });

  // it('Should render <CategoryListC /> inside <FoodCategorySearch />.', () => {
  //   cy.get('#categories-container').should('exist').and('be.visible');
  //   cy.get('#categories-container').then((elements) => {
  //     const categoriesButtons = elements.find('#category-button');
  //     expect(categoriesButtons).to.exist;
  //   });
  // });

  // it('Should render <Footer />.', () => {
  //   cy.get('#footer-container').should('exist').and('be.visible');
  //   cy.get('.grid-cols-6 > #foodies-logo > a')
  //     .should('exist')
  //     .and('be.visible');

  //   cy.get('#btns-footer-containers').then((elements) => {
  //     const btnsFooter = elements.find('img');
  //     expect(btnsFooter).to.exist;
  //   });

  //   cy.get('#footer-routes-container').should('exist').and('be.visible');
  // });
});
