describe('Landing page tests', () => {
  beforeEach(() => {
    cy.visit('/');
    cy.viewport(1300, 750);
  });

  // it('Should show the Landing page components.', () => {
  //   cy.get('#foodies-logo > a').should('exist').and('be.visible');

  //   cy.get('#banner-text-container').then((elements) => {
  //     const bannerText = elements.find('h1').text();
  //     const bannerSubText = elements.find('p').text();

  //     expect(bannerText).to.exist;
  //     expect(bannerSubText).to.exist;
  //   });
  // });

  // it('Should render <WhoIsFoodies />.', () => {
  //   cy.get('#who-is-foodies-section').then((elements) => {
  //     const whoIsFoodiesText = elements.find('h1').text();
  //     const whoIsFoodiesText2 = elements.find('h3').text();
  //     const whoIsFoodiesSubText = elements.find('p').text();

  //     expect(whoIsFoodiesText).to.exist;
  //     expect(whoIsFoodiesText2).to.exist;
  //     expect(whoIsFoodiesSubText).to.exist;
  //   });
  // });

  // it('Should render <SearchPlaces />.', () => {
  //   cy.get('#searchplaces-container').then((elements) => {
  //     const searchPlacesText = elements.find('h1');
  //     const searchPlacesSubText = elements.find('p');

  //     expect(searchPlacesText).to.exist;
  //     expect(searchPlacesSubText).to.exist;

  //     cy.get('#map-image-container').should('exist').and('be.visible');
  //     cy.get('#search-results-container').should('exist').and('be.visible');
  //   });
  // });

  // it('Should render <SliderComponent />.', () => {
  //   cy.get('#testimonials-container').then((elements) => {
  //     const testimonialsText = elements.find('h1').text();
  //     const testimonialsSubText = elements.find('p').text();

  //     expect(testimonialsText).to.exist;
  //     expect(testimonialsSubText).to.exist;

  //     cy.get('#vector-a-slider').should('exist').and('be.visible');
  //     cy.get('#vector-b-slider').should('exist').and('be.visible');
  //     cy.get('#vector-c-slider').should('exist').and('be.visible');
  //   });
  // });

  // it('Should render <FeedbackForm />.', () => {
  //   cy.get('#feedback-container').should('exist').and('be.visible');

  //   cy.get('h1').should('contain', 'Cuentanos tu experiencia');
  //   cy.get('p').should('contain', `Don't miss out on our great offers`);
  //   cy.get('input').should('exist').and('be.visible');
  //   cy.get('textarea').should('exist').and('be.visible');

  //   cy.get('#btn-form-submit').should('exist').and('be.visible').click();

  //   cy.get('#letter-image-feedback').should('exist').and('be.visible');
  //   cy.get('h1')
  //     .should('contain', 'Gracias por tu comentario')
  //     .and('be.visible');
  //   cy.get('p')
  //     .should('contain', `Don't miss out on our great offers`)
  //     .and('be.visible');
  //   cy.get('#btn-form-back').should('exist').and('be.visible').click();
  // });

  // it('Should render <FoodiesApp />', () => {
  //   cy.get('#foodies-app-container').should('exist').and('be.visible');
  //   cy.get('#foodies-app-vector').should('exist').and('be.visible');
  //   cy.get('h1').should('contain', 'Obten más beneficios');
  //   cy.get('#foodies-app-vector-2').should('exist').and('be.visible');

  //   cy.get('#foodies-heading-container').then((elements) => {
  //     const foodiesAppText = elements.find('h1').text();
  //     expect(foodiesAppText).to.exist;
  //   });

  //   cy.get('#foodies-app-steps').should('exist').and('be.visible');
  //   cy.get('#foodies-app-steps-2').should('exist').and('be.visible');
  // });

  // it.only('Should render <Footer />.', () => {
  //   cy.get('#footer-container', { timeout: 10000 })
  //     .should('exist')
  //     .and('be.visible');
  //   cy.get('.grid-cols-6 > #foodies-logo > a')
  //     .should('exist')
  //     .and('be.visible');

  //   cy.get('#btns-footer-containers').then((elements) => {
  //     const btnsFooter = elements.find('img');
  //     expect(btnsFooter).to.exist;
  //   });

  //   cy.get('#footer-routes-container').should('exist').and('be.visible');
  // });
});
